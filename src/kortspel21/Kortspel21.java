
package kortspel21;

import java.util.Random;
import java.util.Scanner;


    public class Kortspel21 {
    
    
    public static void main(String[] args) {
    
        
    //Här börjar spelet med text och sedan ett anrop till game metoden.    
    System.out.println("======================");   
    System.out.println("Det här är kortspel 21");
    System.out.println("======================");    
    Kortspel21.game();
    
    
    }
    
    public static void replay(){
    
    //Denna metod anropas om spelaren väljer att spela igen
    System.out.println("======================");   
    System.out.println("Det här är kortspel 21");
    System.out.println("======================");
    Kortspel21.game();
    
    }
    public static void game(){
        
    Scanner scan = new Scanner(System.in);
    Random dice = new Random();
    
    //Variabler som håller det totala värdet på användaren, och dealerns kort
    int cardTotalValue = 0;
    int dealerTotalValue = 0;
    
    int number;
    
    
    
    //Denna iteration snurrar så länge spelaren väljer att ta ett kort till. För varje nytt kort så ökar spelarens värde som hålls i variabeln cardTotalValue.
    //Jag valde DO-WHILE eftersom jag vet att iterationen skall användas minst en gång.
    int o = 0;
    do
    {    
    o++;    
    //Slumpar ett tal mellan 1-13     
    number = ((int) (Math.random()*13))+1;
    //Ökar värdet på variabeln(number) varje gång det slumpas ett tal 
    cardTotalValue = number + cardTotalValue;
        
       
        
    System.out.println("Ditt kort är: "+ number);
    System.out.println("Du har nu totalt: "+ cardTotalValue);
        
    if(cardTotalValue > 21)
    {
    o=8;
    System.out.println("Du blev tyvärr tjock.");
    System.out.println("========================================================");
    //Denna meny läser av om användaren väljer att spela igen eller avsluta applikationen.
    System.out.println("Vill du spela igen? Skriv(1), Om du vill avsluta skriv(2)");
    int re = scan.nextInt();
    if(re==1)
    {
    Kortspel21.replay();         
    }
    else if(re==2)
    {
    System.exit(0); 
    }
    
    
    }
    else if(cardTotalValue < 21){
        System.out.println("Vill du ha ett till kort? Skriv(1), Vill du stanna? Skriv(2)");
    }
    
        
    //Drar fler kort om användaren skriver (1)
        
    int answer = scan.nextInt();
    if(answer==1){
             
    }
    else
    {
    o = 8;  
    System.out.println("Dealertime!");
    System.out.println("========================================================");
    }
    }    
    while(o<8);
    
    
    //Denna iterration drar dealerns kort. Dealern måste stanna om dealerTotalValue>=16. 
    int i = 0;
    do
    {
    
    number = ((int) (Math.random()*13))+1;
    dealerTotalValue = number + dealerTotalValue;
        
    System.out.println("Dealerns kort är: "+ number);
    
        
    if(dealerTotalValue < 16){
        
    }  
    else if(dealerTotalValue > 21){
        i=8;
        System.out.println("Dealern sprack");
    }
    else if(dealerTotalValue >= 16){
        i=8;
        
    }
    
    
    }
    while(i<8);
    
    //If-else sats som visar om spelarens eller dealerns värde är närmast 21. Om både dealern och användaren har samma värde blir det oavgjort.
    
    System.out.println("Dealern fick totalt: "+ dealerTotalValue+" Och du fick totalt: "+cardTotalValue);
    if(dealerTotalValue < cardTotalValue || dealerTotalValue > 21){
        System.out.println("Grattis du vann!");
    }
    else if(dealerTotalValue == cardTotalValue){
        System.out.println("Du och dealern fick samma värde. Split");
    }
    else{
        System.out.println("Dealern vann");
    }
    
    //Hoppar till replay metoden om användaren vill spela igen. Och avslutar programmet om användaren skriver 2.
    System.out.println("========================================================");
    System.out.println("Vill du spela igen? Skriv(1), Om du vill avsluta skriv(2)");
    int re = scan.nextInt();
    if(re==1)
    {
    Kortspel21.replay();         
    }
    else if(re==2)
    {
    System.exit(0); 
    }
    }
    
    
}   
        
    
    
    
 
        
        
      
    
    
    
       
    
    
    
    
    
   
    
    
        
         
        
        
    
    
    
    
     
    
    
        
      
        
            
    
        
        
    
